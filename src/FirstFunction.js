import React,{useEffect,useState} from 'react';

function FirstFunction(props) {
    
    const [name,setName]=useState('First Name Anand ');
    const [age,setAge]=useState('Your Age ');

     useEffect(()=>{ console.log('did updated ?',props.name);
    },[age])
    
    useEffect(()=>{
     

     console.log('did use hook propsname ?',props.name); 

    },[props.name])

    
    let data='My First Test Function'
  return(<div> 
      <h5> {data} </h5>
      <h5> props::=>{props.name} </h5>

  <h6>{age}</h6>
  <h6>{name}</h6>
  <button onClick={()=>setName('Name update Anand')} >NAME update </button>
  <button onClick={()=>setAge('Name update 26')} > AGE Update </button>

  </div>)
}

export default FirstFunction;


 //Life cycle  method
//  constructor(){
//     alert("The component named Header is about to be constructor.");
//  }

// componentDidMount(){
//     alert("The component named Header is about to be mount.");
// }

// componentDidUpdate(){
//     alert("The component named Header is about to be update.");
// }

// componentWillUnmount() {
//     alert("The component named Header is about to be unmounted.");
//   }
//