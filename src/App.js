
import './App.css';
import React,{useState} from 'react'; 
import Inputfield from './Inputfield';
import Home from './Home';
import FirstFunction from './FirstFunction'; 
import FirstClass from './FirstClass';
import Contact from './Contact';
import Users from './Users';
import Createuser from './Createuser';
import {Link,Route,BrowserRouter as Router,Switch} from 'react-router-dom';
import {Navbar,Nav} from 'react-bootstrap';
function App() {
const [name1,setName1]=useState('prop update');

  return (
    <div className="App">
      <Router>

  <Navbar bg="primary" variant="dark">
    <Navbar.Brand href="#home">Navbar</Navbar.Brand>
    <Nav className="mr-auto">
      <Nav.Link href="#home">  <Link to="/" >Home</Link></Nav.Link>
      <Nav.Link href="#features"><Link to="contact" >Contact us</Link></Nav.Link>
      <Nav.Link href="#"> <Link to="firstclass" >FirstClass</Link></Nav.Link>
      <Nav.Link href="#"><Link to="firstfunction" >FirstFunction</Link></Nav.Link>
      <Nav.Link href="#"><Link to="users" >Users</Link></Nav.Link>
      <Nav.Link href="#"><Link to="createuser" >Createuser</Link></Nav.Link>
    </Nav>
  </Navbar>

  <Switch>
       
          <Route path="/contact">
            <Contact />
          </Route>
          <Route path="/firstclass">
            <FirstClass name={name1} />
          </Route>
          <Route path="/firstfunction">
            <FirstFunction  name={name1} />
          </Route>
          <Route path="/users">
            <Users />
          </Route>
          <Route path="/createuser">
            <Createuser />
          </Route>
          <Route path="/">
            <Home />
          </Route>
        </Switch>
      </Router>
{/*       
      <header className="App-header">
     
        <p>
         React App Development                
        </p>
        <button onClick={()=>setName1('prop updated') } > Prop Update</button>
      </header> */}
      
    </div>
  );
}

export default App;
