import React from 'react';
import { render } from 'react-dom';

class FirstClass extends React.Component{
    
constructor(){
     super();
    this.state={
        name:"Anand",
        id:55

    } 
    
    // console.log("The component named Header is about to be constructor.");
   }

componentDidMount(){
      
    console.log('did mount props ?');
    console.log(this.props);
   }

   componentDidUpdate(){
    console.log('did updated props?');
    console.log(this.props);
}

    
    render(){
        return(<div>
            <h1> My First Class Component</h1>
        <h2>{this.props.name}</h2>
        <h3> Name: {this.state.name} Id: {this.state.id}</h3>
         <button onClick={()=>{this.setState({name:'onclick Change Anand Kumar '})}} > Button </button>
        </div>)
    }

} 

export default FirstClass;


